Ext.application({
    name: 'TodoList',

    views: [
        'TaskListView',
        'TabPanel'
    ],

    stores: [
        'TasksStore',
    ],

    controllers: [
       'MainController'
    ],

    launch: function() {      
        Ext.fly('appLoadingIndicator').destroy();
        Ext.Viewport.add(Ext.create('TodoList.view.TabPanel'));  
    },
});