Ext.define('TodoList.store.TasksStore', {
    extend: 'Ext.data.Store',
    requires: 'TodoList.model.TaskModel',

    config: {
        autoLoad: true, // automatyczne ładowanie danych po stworzeniu store'a
        autoSync: true,
        
        model: "TodoList.model.TaskModel", // typ przechowywanego modelu

        groupField: "list", // pole, po którym grupowane będą przechowywane obiekty

        // definicja proxy obsługującego pliki JSON
        proxy: {
            type: 'localstorage',
            id  : 'todolist'
        },
    }
});