Ext.define('TodoList.model.TaskModel', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name: 'author', type: 'string' },
            { name: 'title', type: 'string' },
            { name: 'list', type: 'string' },
            { name: 'done', type: 'boolean', defaultValue: false}
        ],
    }
});