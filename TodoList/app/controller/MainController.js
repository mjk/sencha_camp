Ext.define('TodoList.controller.MainController', {
	extend: 'Ext.app.Controller',

	requires: ['Ext.MessageBox'],

	config: {
		refs: {
			taskListView: '#taskListView',
			formsubmit: '#formsubmit',
			tabPanel: '#tabPanel',
		},

		control: {
			taskListView: {
				itemtap: 'onTaskTap',
				itemswipe: 'onTaskSwipe',
				itemtaphold: 'onTaskHold',
			},
			formsubmit: {
				tap: 'onFormSubmit'
			},
		},
	},

	onTaskTap: function(list, index, target, record) {
		Ext.Msg.alert(record.get("title"));
	},

	onTaskSwipe: function(list, index, target, record, evt) {
		if(evt.direction == "right") {
			record.set("done", true);
		} else if (evt.direction == "left") {
			record.set("done", false);
		}
	},

	onTaskHold: function(list, index, target, record) {
		Ext.getStore('TasksStore').remove(record);
		list.refresh();
	},

	onFormSubmit: function(btn) {
		var formValues = btn.up('addTask').getValues();
		Ext.getStore('TasksStore').add(formValues);

		btn.up('addTask').reset();
		this.showTasks();
	},

	showTasks: function() {
		this.getTabPanel().getTabBar().setActiveTab(0);
	},
});