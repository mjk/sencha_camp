Ext.define("TodoList.view.TabPanel", {
   extend: 'Ext.tab.Panel',

   requires: [
      'TodoList.view.TaskListView',
      'TodoList.form.AddTaskForm'
   ],

   id: 'tabPanel',

   config: {
      activeTab: 0, 

      tabBar: {
         docked: 'bottom',
      },

      items: [
         {
            xtype: 'taskListView',
            iconCls: 'list'
         },
         {
             xtype: 'addTask',
             iconCls: 'add'
         },
      ]
   }
});